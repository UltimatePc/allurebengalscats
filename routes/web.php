<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'GuestPagesController@index')->name('index');
Route::get('/Kings', 'GuestPagesController@kings')->name('kings');
Route::get('/Adults', 'GuestPagesController@adults')->name('adults');
Route::get('/Queens', 'GuestPagesController@queens')->name('queens');
Route::get('/Gallery', 'GuestPagesController@gallery')->name('gallery');
Route::get('/Kittens', 'GuestPagesController@kittens')->name('kittens');
Route::get('/AboutUs', 'GuestPagesController@aboutUs')->name('aboutUs');
Route::get('/ContactUs', 'GuestPagesController@contactUs')->name('contactUs');

Route::get('/Inicio', 'GuestPagesController@esIndex')->name('esIndex');
Route::get('/Reyes', 'GuestPagesController@esKings')->name('esKings');
Route::get('/Reinas', 'GuestPagesController@esQueens')->name('esQueens');
Route::get('/Adultos', 'GuestPagesController@esAdults')->name('esAdults');
Route::get('/Galeria', 'GuestPagesController@esGallery')->name('esGallery');
Route::get('/Gatitos', 'GuestPagesController@esKittens')->name('esKittens');
Route::get('/Nosotros', 'GuestPagesController@esAboutUs')->name('esAboutUs');
Route::get('/Contactenos', 'GuestPagesController@esContactUs')->name('esContactUs');
