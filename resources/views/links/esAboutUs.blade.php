@extends('layouts.guest')

@section('title', 'Nosotros')
@section('id', 'Allure Bengals')
@section('pageName', 'Nosotros')
@section('langSwitch', '/AboutUs')

@include('components.esNavbar', ['active' => 'De Nosotros'])

@section('content')

<div class="container-fluid mt-5">

  <div class="row no-gutters my-5">
    <div class="col">
      <h1 class="text-light text-center text-title">Sobre Nosotros</h1>
    </div>
  </div>

<div class="container text-center">
    <h4 class="text-white mt-5">
    Bienvenido al sitio web de Allure Bengals. Agradecemos que se tome el tiempo para ver a nuestros gatos y gatitos. Allure Bengals es un criadero registrado por TICA. Nuestra familia ha estado trabajando con gatos de Bengala durante casi veinte años. Nuestro objetivo principal siempre ha sido producir Bengals de calidad superior con temperamentos excelentes. Esto se logra mediante la cría selectiva. Elegimos cualidades tanto en estructura como en personalidad para ayudar a producir Bengals de mayor calidad.
    <br> <br> A lo largo del camino, hemos tenido mentores que nos ayudaron a guiarnos y seleccionar los mejores gatos criadores para trabajar. Expresamos nuestra extrema gratitud por ser mentores de algunos de los mejores criadores de Bengala de la nación. Te agradecemos.
    <br> <br> El año pasado nos mudamos y compramos una casa en Honaker Va., que se encuentra en las hermosas montañas de Virginia. Allure Bengals ofrece servicios en el norte de Virginia, Maryland, Carolina del Norte, Tennessee, Virginia Occidental y Carolina del Sur. También transportaremos a nivel nacional. Criamos con un nivel de calidad extremadamente alto. Nuestros gatos reproductores se examinan anualmente para detectar afecciones que afectan al gato de Bengala y las enfermedades de los gatos en general. Una diferencia importante entre Allure Bengals y otros criadores de gatos es que ofrecemos una garantía de salud del 100%. Esto incluye una garantía de defectos genéticos de por vida sin precedentes en la mayoría de los criaderos. Ofrecemos esta garantía para uno porque tenemos un alto grado de confianza en que nuestros Bengals están libres de enfermedades, ya que nuestras líneas con las que hemos trabajado durante años. Pero si algo surgiera inesperadamente, Allure Bengals se compromete a brindarle un gato o gatito saludable.
    <br> <br> Amamos mucho a nuestros gatos. Todos son considerados nuestras mascotas. Los gatitos se crían adentro con nosotros en un ambiente hogareño lleno de amor. Pasamos la mayor parte del día cuidando y socializando a los jóvenes desde que nacen. Su gatito estará al día con las vacunas y será desparasitado. Los gatitos se prueban completamente en cajas de arena antes de dejarnos. Pasan por diferentes etapas de desarrollo mientras están aquí con nosotros para asegurar un gatito bien socializado. Nunca dejamos que un gatito se vaya hasta que esté listo tanto física como socialmente. Las reservas son 500 dólares por gatito. Las tarifas de adopción son de 1800 a 3500 dólares. Ofrecemos descuentos militares y múltiples descuentos para gatitos. Si está buscando un gato mayor, verifique si tenemos gatos que se retiran disponibles. ¡Son la crema de la cosecha!
    <br> <br> Si está buscando ser incluido en la lista de reserva o si tiene alguna pregunta. ¡Esperamos con interés escuchar de usted!
    <br>
    <br> <br> Gracias,
    <br><br>Zachary Lexington Dunlap
    <br>(919)308-4684
    <br>Honaker V.A
    </h4>
    <h4 class="text-white mt-5">
      -Allure Bengals
    </h4>
    <h4 class="m-5">
      <a href="mailto:Allurebengals@gmail.com" class="text-white">Allurebengals@gmail.com</a>
    </h4>
  </div>
</div>

@include('components.esFooter')
@endsection
