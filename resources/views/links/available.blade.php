@extends('layouts.guest')

@section('title', 'Available')
@section('id', 'Allure Bengals')
@section('pageName', 'Available')
@section('langSwitch', '/Disponible')

@include('components.navbar', ['active' => 'Available'])

@section('content')

<div class="container-fluid mb-5 mt-5">
  
</div>

@include('components.footer')
@endsection
