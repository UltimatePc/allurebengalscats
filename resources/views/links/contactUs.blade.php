@extends('layouts.guest')

@section('title', 'Contact Us')
@section('id', 'Allure Bengals')
@section('pageName', 'Contact Us')
@section('langSwitch', '/Contactenos')

@include('components.navbar', ['active' => 'Contact Us'])

@section('content')

<div class="container-fluid d-flex justify-content-center text-center text-light mt-5">
  <div class="row">
    <div class="col-12 col-md-6">
      <img data-src="/imgs/heros/allure_bengals_cat_11.JPG" class="d-block w-100 rounded lazy" alt="Allure Bengal Cats">
    </div>
    <div class="col-12 col-md-6 mt-5 pt-5">
      <h1 class="text-title mb-5">Contact Us</h1>
      <div class="row">
        <div class="col-12 col-md-6 d-flex align-items-center justify-content-center my-2">
          <i class="fas fa-phone-alt fa-2x text-light"></i><p class="h4"> &nbsp; (919) 308-4684 (Speak with Zack)</p>
        </div>
        <div class="col-12 col-md-6 d-flex align-items-center justify-content-center my-2">
          <i class="fas fa-phone-alt fa-2x text-light"></i><p class="h4"> &nbsp; (276) 732-0277 (Speak with Evyenia)</p>
        </div>
      </div>
      <div class="row">
        <div class="col d-flex align-items-center justify-content-center my-2">
          <i class="fas fa-envelope fa-2x"></i><a class="text-light" href="mailto:allurebengals@gmail.com"> &nbsp; AllureBengals@gmail.com</a>
        </div>
        <div class="col d-flex align-items-center justify-content-center my-2">
          <a  class="text-light" href="https://www.instagram.com/allurebengals/"><i class="fab fa-instagram fa-2x"></i> &nbsp; instagram.com/allurebengals/</a>
        </div>
      </div>
      <div class="row">
        <div class="col d-flex align-items-center justify-content-center my-2">
          <a  class="text-light" href="http://www.facebook.com/allurebengals"><i class="fab fa-facebook-square fa-2x"></i> &nbsp; facebook.com/allurebengals</a>
        </div>
        <div class="col d-flex align-items-center justify-content-center my-2">
          <a  class="text-light" href="https://twitter.com/AllureBengals"><i class="fab fa-twitter-square fa-2x"></i> &nbsp; twitter.com/AllureBengals</a>
        </div>
      </div>
      <div class="row">
        <div class="col d-flex align-items-center justify-content-center my-2">
          <p>Air transportation is available in the U.S and Internationally. We provide safe, very reliable air-transportation via Petsafe, Delta, and United airlines</p>
        </div>
      </div>
    </div>
  </div>
</div>

@include('components.footer')
@endsection
