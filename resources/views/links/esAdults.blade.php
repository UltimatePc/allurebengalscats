@extends('layouts.guest')

@section('title', 'Adultos')
@section('id', 'Allure Bengals')
@section('pageName', 'Adultos')
@section('langSwitch', '/Adults')

@include('components.esNavbar', ['active' => 'Gatos Adultos'])

@section('content')

<div class="container-fluid mt-5">
    <div class="row mb-3 mb-md-0 no-gutters">
    <div class="col">
        <h1 class="text-light mb-5 text-center text-title">Gatos Disponibles</h1>
    </div>
    </div>

    <!-- Calleum -->
    <div class="row mb-3 mb-md-0 no-gutters bg-light">
    <div class="col-12 col-md-6 text-black d-flex align-items-center">
    <div class="container text-box text-center text-md-right mb-5">
        <h1 class="m-5 text-title">Calleum</h1>
        <p class="mx-md-5">
        Padre- Gatos de la isla de Bengala Winterberry<br>
        Madre- Reino de los Leopardos Matisse del Atlántico<br>
        Descripción: Calleum es un impresionante macho tricolor con rosetas de manchas marrones. Él tiene
        sido castrado. Es un chico muy amable; Le encanta que lo acaricien y es meticuloso.
        Hábitos de la caja de arena. Le iría muy bien en cualquier entorno hogareño familiar. Calleum
        tiene aproximadamente 5 años.
        </p>
    </div>
    </div>
    <div class="col-12 col-md-6 order-first order-md-last">
        <img src="/imgs/adults/Allure_Bengals_Calleum.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    </div>

    <!-- Emma -->
    <div class="row mb-3 mb-md-0 no-gutters">
    <div class="col-12 col-md-6">
        <img src="/imgs/adults/Allure_Bengals_Emma.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    <div class="col-12 col-md-6 text-light d-flex align-items-center">
        <div class="container text-box text-center text-md-left mb-5">
        <h1 class="m-5 text-title">Emma</h1>
        <p class="mx-md-5">
        Padre- Raffa<br>
        Madre- Bella<br>
        Descripción- Emma es una preciosa hembra con manchas marrones. ella tiene aproximadamente 6 años
        años. Tiene una cabeza de Bengala muy salvaje y distintas rosetas con manchas marrones.
        que están dispuestos en un patrón horizontal perfecto. Es una niña dulce y muy cariñosa.
        </p>
        </div>
    </div>
    </div>

    <!-- Mocha -->
    <div class="row mb-3 mb-md-0 no-gutters bg-light">
    <div class="col-12 col-md-6 text-black d-flex align-items-center">
        <div class="container text-box text-center text-md-right mb-5">
        <h1 class="m-5 text-title">Mocha</h1>
        <p class="mx-md-5">
        Padre- London<br>
        Madre- Naudia<br>
        Descripción: Mocha es una hembra de Bengala con manchas de color marrón oscuro. Ella es aproximadamente una
        años de edad. Sus bigotes son increíbles y enormes. Su personalidad es tan dulce.
        Mocha rueda pidiendo mascotas. Ella será una gran incorporación a cualquier familia.
        situación.
        </p>
        </div>
    </div>
    <div class="col-12 col-md-6 order-first order-md-last">
        <img src="/imgs/adults/Allure_Bengals_Mocha.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    </div>

    <!-- Sasha -->
    <div class="row mb-3 mb-md-0 no-gutters">
        <div class="col-12 col-md-6">
        <img src="/imgs/adults/Allure_Bengals_Sasha.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
        </div>
        <div class="col-12 col-md-6 text-light d-flex align-items-center">
        <div class="container text-box text-center text-md-left mb-5">
            <h1 class="m-5 text-title">Sasha</h1>
            <p class="mx-md-5">
            Padre- Jasmine<br>
            Madre- Orion<br>
            Descripción- Sasha tiene alrededor de un año y medio. Ella es una roseta tricolor.
            Bengala femenina. Sus rosetas son anilladas y tienen un raro color jengibre en el interior.
            Ella es una Bengala muy relajada, no dominante. Tiene un ligero defecto en el pie debido a una
            Problema del cordón umbilical durante el parto. Tiene un pie intacto y tiene bastante buena
            movilidad. Esto no restringe de ninguna manera su vida diaria. Ella haría mejor en un
            hogar con un solo gato.
            </p>
        </div>
        </div>
    </div>

    <!-- Sadie -->
    <div class="row mb-3 mb-md-0 no-gutters bg-light">
    <div class="col-12 col-md-6 text-black d-flex align-items-center">
        <div class="container text-box text-center text-md-right mb-5">
        <h1 class="m-5 text-title">Sadie</h1>
        <p class="mx-md-5">
        Padre- Jasmine<br>
        Madre- Orion<br>
        Descripción- Sadie es una hermosa hembra de Bengala con rosetas marrones y un tono pelirrojo.
        Es muy amigable y tiene un gran temperamento. A Sadie le encantan las mascotas, las golosinas y
        buena sesión de juego. Tiene aproximadamente 2 años.
        </p>
        </div>
    </div>
    <div class="col-12 col-md-6">
        <img src="/imgs/adults/Allure_Bengals_Sadie.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    </div>
    </div>

@include('components.esFooter')
@endsection
