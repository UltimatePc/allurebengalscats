@extends('layouts.guest')

@section('title', 'Reinas')
@section('id', 'Allure Bengals')
@section('pageName', 'Reinas')
@section('langSwitch', '/Queens')

@include('components.esNavbar', ['active' => 'Reinas'])

@section('content')

<div class="container-fluid mt-5">
  <div class="row mb-3 mb-md-0 no-gutters">
    <div class="col">
      <h1 class="text-light mb-5 text-center text-title">Las Reinas</h1>
    </div>
  </div>

    <!-- Cha Cha -->
    <div class="row mb-3 mb-md-0 no-gutters bg-light" id="ChaCha">
    <div class="col-12 col-md-6 text-black d-flex align-items-center">
      <div class="container text-box text-center text-md-right mb-5">
        <h1 class="m-5 text-title">Cha Cha</h1>
        <p class="mx-md-5">
        Cha Cha es una de nuestras reinas del carbón. Ella está fuera de Raven y Orion.
        Tiene increíbles gatitos de carbón y es una madre maravillosa.
        A Cha cha le encanta tener conversaciones y tiene un temperamento tan maravilloso.
        Siempre esperamos a sus bebés de carbón, son una alegría.
        </p>
      </div>
    </div>
    <div class="col-12 col-md-6 order-first order-md-last">
      @include('components.carousel.QueensChacha')
    </div>
  </div>

  <!-- Khaleesi -->
  <div class="row mb-3 mb-md-0 no-gutters" id="Khaleesi">
    <div class="col-12 col-md-6">
      @include('components.carousel.QueensKhaleesi')
    </div>
    <div class="col-12 col-md-6 text-light d-flex align-items-center">
      <div class="container text-box text-center text-md-left mb-5">
        <h1 class="m-5 text-title">Khaleesi</h1>
        <p class="mx-md-5">
          Madre de Dragones Khaleesi.
          Su fuerte personalidad y temperamento la han convertido en una de nuestras reinas favoritas.
          Sus gatitos tienen todas las características maravillosas y los rasgos de personalidad que tiene.
        </p>
      </div>
    </div>
  </div>

  <!-- Tessa -->
  <div class="row mb-3 mb-md-0 no-gutters bg-light" id="Tessa">
    <div class="col-12 col-md-6 text-black d-flex align-items-center">
      <div class="container text-box text-center text-md-right mb-5">
        <h1 class="m-5 text-title">Tessa</h1>
        <p class="mx-md-5">
          Aún no hay biografía
        </p>
      </div>
    </div>
    <div class="col-12 col-md-6 order-first order-md-last">
      @include('components.carousel.QueensTessa')
    </div>
  </div>

  <!-- Bella -->
  <div class="row mb-3 mb-md-0 no-gutters" id="Bella">
    <div class="col-12 col-md-6">
      @include('components.carousel.QueensBella')
    </div>
    <div class="col-12 col-md-6 text-light d-flex align-items-center">
      <div class="container text-box text-center text-md-left mb-5">
        <h1 class="m-5 text-title">Bella</h1>
        <p class="mx-md-5">
          Bella es de nuestros criadores exóticos hiponóticos.
          Su linaje es muy fuerte. El padre de Bellas es el Capitán Jack Sparrow.
          Ella tiene increíbles gatitos de carbón.
        </p>
      </div>
    </div>
  </div>

  <!-- Rosemary -->
  <div class="row mb-3 mb-md-0 no-gutters bg-light" id="Rosemary">
    <div class="col-12 col-md-6 text-black d-flex align-items-center">
      <div class="container text-box text-center text-md-right mb-5">
        <h1 class="m-5 text-title">Rosemary</h1>
        <p class="mx-md-5">
          Rosemary es una reina de rosetón marrón.
           Ella tiene un contraste y color sobresalientes.
           Su piel tiene un flujo muy natural.
           Sin mencionar que es una chica muy dulce y cariñosa.
           Estamos encantados de que ella sea parte de nuestro programa.
        </p>
      </div>
    </div>
    <div class="col-12 col-md-6 order-first order-md-last">
      @include('components.carousel.QueensRosemary')
    </div>
  </div>

  <!-- Lavender -->
  <div class="row mb-3 mb-md-0 no-gutters" id="Lavender">
    <div class="col-12 col-md-6">
      @include('components.carousel.QueensLavender')
    </div>
    <div class="col-12 col-md-6 text-light d-flex align-items-center">
      <div class="container text-box text-center text-md-left mb-5">
        <h1 class="m-5 text-title">Lavender</h1>
        <p class="mx-md-5">
          Aún no hay biografía
        </p>
      </div>
    </div>
  </div>

  <!-- Spice -->
  <div class="row mb-3 mb-md-0 no-gutters mb-5 bg-light" id="Spice">
    <div class="col-12 col-md-6 text-black d-flex align-items-center">
      <div class="container text-box text-center text-md-right mb-5">
        <h1 class="m-5 text-title">Spice</h1>
        <p class="mx-md-5">
          Aún no hay biografía
        </p>
      </div>
    </div>
    <div class="col-12 col-md-6 order-first order-md-last">
      @include('components.carousel.QueensSpice')
    </div>
  </div>

  <!-- Jasmine -->
  <div class="row mb-3 mb-md-0 no-gutters" id="Jasmine">
    <div class="col-12 col-md-6">
      @include('components.carousel.QueensJasmine')
    </div>
    <div class="col-12 col-md-6 text-light d-flex align-items-center">
      <div class="container text-box text-center text-md-left mb-5">
        <h1 class="m-5 text-title">Jasmine</h1>
        <p class="mx-md-5">
          Aún no hay biografía
        </p>
      </div>
    </div>
  </div>

  <!-- Raven -->
  <div class="row mb-3 mb-md-0 no-gutters mb-5 bg-light" id="Raven">
    <div class="col-12 col-md-6 text-black d-flex align-items-center">
      <div class="container text-box text-center text-md-right mb-5">
        <h1 class="m-5 text-title">Raven</h1>
        <p class="mx-md-5">
          Aún no hay biografía
        </p>
      </div>
    </div>
    <div class="col-12 col-md-6 order-first order-md-last">
      @include('components.carousel.QueensRaven')
    </div>
  </div>

</div>

@include('components.esFooter')
@endsection
