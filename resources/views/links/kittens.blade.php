@extends('layouts.guest')

@section('title', 'Kittens')
@section('id', 'Allure Bengals')
@section('pageName', 'Kittens')
@section('langSwitch', '/Gatitos')

@include('components.navbar', ['active' => 'Kittens'])

@section('content')

<div class="container-fluid mt-5">
  <div class="row mb-3 mb-md-0 no-gutters">
  <div class="col">
    <h1 class="text-light mb-5 text-center text-title">Available Kittens</h1>
  </div>
  </div>

  <!-- Male #1 -->
  <div class="row mb-3 mb-md-0 no-gutters bg-light">
  <div class="col-12 col-md-6 text-black d-flex align-items-center">
    <div class="container text-box text-center text-md-right mb-5">
      <h1 class="m-5 text-title">Brown Rosetted Male # 1</h1>
      <p class="mx-md-5">
      Naudia And London had 4 beautiful Brown rossetted charcoal kittens on June 7th. Naudia is being a great mother as usual. 
      They have beautiful markings with very high contrast. We can't wait to see these little guy's grow. 
      Please contact us for Availability!
      </p>
    </div>
  </div>
  <div class="col-12 col-md-6 order-first order-md-last">
  <img src="/imgs/kittens/Allure_Bengals_Male_1.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
  </div>
  </div>

  <!-- Male #2 -->
  <div class="row mb-3 mb-md-0 no-gutters">
  <div class="col-12 col-md-6">
  <img src="/imgs/kittens/Allure_Bengals_Male_2.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
  </div>
  <div class="col-12 col-md-6 text-light d-flex align-items-center">
    <div class="container text-box text-center text-md-left mb-5">
      <h1 class="m-5 text-title">Brown Rosetted Male # 2</h1>
      <p class="mx-md-5">
      Sire- Cisco<br>
      Dam- Maya<br>
      Description- This male is one of our most stunning kittens available. 
      He is easily breeder quality due to his color, pattern and head type. 
      His personality like his Sire Cisco, very easy going, so loveable as a pet. 
      He is approximately 5 months old
      </p>
    </div>
  </div>
  </div>

  <!-- Male #3 -->
  <div class="row mb-3 mb-md-0 no-gutters bg-light">
    <div class="col-12 col-md-6 text-black d-flex align-items-center">
      <div class="container text-box text-center text-md-right mb-5">
        <h1 class="m-5 text-title">Male Kitten # 3</h1>
        <p class="mx-md-5">
        Sire- Prince<br>
        Dam- Rosemary<br>
        Description- This little guy is 4 weeks old. 
        He has a wonderful personality and temperament for his age. 
        He Has a little spice to him. Bunny hops around and is currently going through his developmental stages well.
        </p>
    </div>
  </div>
    <div class="col-12 col-md-6 order-first order-md-last">
      <img src="/imgs/kittens/Allure_Bengals_Male_3.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
  </div>

  <!-- Male #4 -->
  <div class="row mb-3 mb-md-0 no-gutters">
    <div class="col-12 col-md-6">
    <img src="/imgs/kittens/Allure_Bengals_Male_4.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    <div class="col-12 col-md-6 text-light d-flex align-items-center">
      <div class="container text-box text-center text-md-left mb-5">
        <h1 class="m-5 text-title">Male Kitten # 4</h1>
        <p class="mx-md-5">
        Sire- Prince<br>
        Dam- Rosemary<br>
        Description- This baby boy is a litter mate to kitten #3. 
        He also is developing very well. 
        They love to play together and would do good together If you were looking for two kittens. 
        He has a great temperament and will make a wonderful pet.
        </p>
      </div>
    </div>
  </div>
</div>


@include('components.footer')
@endsection
