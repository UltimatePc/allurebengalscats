@extends('layouts.guest')

@section('title', 'Contáctenos')
@section('id', 'Allure Bengals')
@section('pageName', 'Contáctenos')
@section('langSwitch', '/ContactUs')

@include('components.esNavbar', ['active' => 'Contáctenos'])

@section('content')

<div class="container-fluid d-flex justify-content-center text-center text-light mt-5">
  <div class="row">
    <div class="col-12 col-md-6">
      <img data-src="/imgs/heros/allure_bengals_cat_11.JPG" class="d-block w-100 rounded lazy" alt="Allure Bengal Cats">
    </div>
    <div class="col-12 col-md-6 mt-5 pt-5">
      <h1 class="text-title mb-5">Contáctenos</h1>
      <div class="row">
        <div class="col-12 col-md-6 d-flex align-items-center justify-content-center my-2">
          <i class="fas fa-phone-alt fa-2x text-light"></i><p class="h4"> &nbsp; (919) 308-4684 (Hablar con  Zack)</p>
        </div>
        <div class="col-12 col-md-6 d-flex align-items-center justify-content-center my-2">
          <i class="fas fa-phone-alt fa-2x text-light"></i><p class="h4"> &nbsp; (276) 732-0277 (Hablar con Evyenia)</p>
        </div>
      </div>
      <div class="row">
        <div class="col d-flex align-items-center justify-content-center my-2">
          <i class="fas fa-envelope fa-2x"></i><a class="text-light" href="mailto:allurebengals@gmail.com"> &nbsp; AllureBengals@gmail.com</a>
        </div>
        <div class="col d-flex align-items-center justify-content-center my-2">
          <a  class="text-light" href="https://www.instagram.com/allurebengals/"><i class="fab fa-instagram fa-2x"></i> &nbsp; instagram.com/allurebengals/</a>
        </div>
      </div>
      <div class="row">
        <div class="col d-flex align-items-center justify-content-center my-2">
          <a  class="text-light" href="http://www.facebook.com/allurebengals"><i class="fab fa-facebook-square fa-2x"></i> &nbsp; facebook.com/allurebengals</a>
        </div>
        <div class="col d-flex align-items-center justify-content-center my-2">
          <a  class="text-light" href="https://twitter.com/AllureBengals"><i class="fab fa-twitter-square fa-2x"></i> &nbsp; twitter.com/AllureBengals</a>
        </div>
      </div>
      <div class="row">
        <div class="col d-flex align-items-center justify-content-center my-2">
          <p>El transporte aéreo está disponible en los EE. UU. E internacionalmente. Brindamos transporte aéreo seguro y muy confiable a través de Petsafe, Delta y United Airlines.</p>
        </div>
      </div>
    </div>
  </div>
</div>

@include('components.esFooter')
@endsection
