@extends('layouts.guest')

@section('title', 'Home')
@section('id', 'Allure Bengals')
@section('pageName', 'Home')
@section('langSwitch', '/Inicio')

@include('components.navbar', ['active' => 'Home'])

@section('content')

<div class="container-fluid mb-5 mt-5">
  <div class="row">
    <div class="col-12 col-md-5 d-flex text-center align-items-center justify-content-center">
      <h4 style="font-style: italic">
        <img data-src="/imgs/logos/allure_bengals.png" class="d-block w-100 lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
        <a href="{{ route('kittens') }}">
          <button type="button" class="btn btn-warning btn-lg mb-4 text-primary">
            View Available Kittens
          </button>
        </a>
        <a href="{{ route('adults') }}">
          <button type="button" class="btn btn-warning btn-lg mb-4 text-primary">
            View Available Adults
          </button>
        </a>
      </h4>
    </div>
    <div class="col-12 col-md-7">
      @include('components.carousel.IndexHero')
    </div>
  </div>
</div>

<div class="container-fluid bg-white">
  <div class="row d-flex text-center align-items-center justify-content-center">
    <div class="col-12 col-md-6 pt-5 pb-5">
      <img data-src="/imgs/heros/allure_bengals_cat_60.jpg" class="d-block w-100 rounded lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    <div class="col-12 col-md-6 mt-5 mb-5">
      <h1 class="mb-3 text-title"><b>Looking for the perfect Bengal kitten?</b></h1>
      <h2 class="mb-3 text-title">Allure Bengals welcomes all</h2>
      <p>Allure Bengals is a small family run cattery located in the rolling hills of Virginia. We are a TICA registered cattery. Allure Bengal's kittens are backed
        with a lifetime genetic health guarantee. Our cats and kittens are raised in a loving home environment, and are usually ready to leave between 8 and 12 weeks of age.
        before they leave for their new homes, all of our kittens receive their first series of vaccinations, and are completely litter box trained. We expose our kittens to many
        typical household activities, our other pets, and lots of visitors in order to achieve the best social development possible.
        <br><br><img data-src="/imgs/logos/TICA.jpg" class="lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com"><br><br>
        Our family has a passion for Bengals, and a love for how the many ways they can change your life! Please <a href="{{ route('contactUs') }}">Contact Us</a>  if you would like more Information on how to make one a
        part of yours! <i class="fas fa-heart" style="color: red;"></i>
      </p>
    </div>
  </div>
</div>

<div class="container-fluid mt-5 pt-5 text-light text-center">
  <h3 class="mb-5">Contact Information</h3>
  <div class="row">
    <div class="col d-flex align-items-center justify-content-center my-2">
      <i class="fas fa-envelope fa-2x"></i> &nbsp; &nbsp; <a class="text-light" href="mailto:allurebengals@gmail.com"> allurebengals@gmail.com </a>
    </div>
  </div>
  <div class="row">
    <div class="col d-flex align-items-center justify-content-center my-2">
      <i class="fas fa-phone-alt fa-2x text-light"></i> &nbsp; &nbsp; <h4> (919) 308-4684 (Speak with Zack)</h4>
    </div>
  </div>
  <div class="row">
    <div class="col d-flex align-items-center justify-content-center my-2">
      <i class="fas fa-phone-alt fa-2x text-light"></i> &nbsp; &nbsp; <h4> (276) 732-0277 (Speak with Evyenia)</h4>
    </div>
  </div>
  <div class="row">
    <div class="col d-flex align-items-center justify-content-center my-2">
      <i class="fab fa-facebook-square fa-2x"></i> &nbsp; &nbsp; <a class="text-light" href="http://www.facebook.com/allurebengals"> facebook.com/allurebengals </a>
    </div>
  </div>
  <div class="row">
    <div class="col d-flex align-items-center justify-content-center my-2">
      <i class="fab fa-instagram fa-2x"></i> &nbsp; &nbsp; <a class="text-light" href="https://www.instagram.com/allurebengals/"> instagram.com/allurebengals/ </a>
    </div>
  </div>
  <div class="row">
    <div class="col d-flex align-items-center justify-content-center my-2">
      <i class="fab fa-twitter-square fa-2x"></i> &nbsp; &nbsp; <a class="text-light" href="https://twitter.com/AllureBengals"> twitter.com/AllureBengals </a>
    </div>
  </div>
  <div class="row">
    <div class="col d-flex align-items-center justify-content-center my-2">
      <p>Air transportation is available in the U.S and Internationally. We provide safe, very reliable air-transportation via Petsafe, Delta, and United airlines</p>
    </div>
  </div>
</div>

@include('components.footer')
@endsection
