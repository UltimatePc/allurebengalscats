@extends('layouts.guest')

@section('title', 'Inicio')
@section('id', 'Allure Bengals')
@section('pageName', 'Inicio')
@section('langSwitch', '/')

@include('components.esNavbar', ['active' => 'Inicio'])

@section('content')

<div class="container-fluid mb-5 mt-5">
  <div class="row">
    <div class="col-12 col-md-5 d-flex text-center align-items-center justify-content-center">
      <h4 style="font-style: italic">
        <img data-src="/imgs/logos/allure_bengals.png" class="d-block w-100 lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
        <a href="{{ route('esKittens') }}">
          <button type="button" class="btn btn-warning btn-lg mb-4 text-primary">
            Ver Gatitos Disponibles
          </button>
        </a>
        <a href="{{ route('esAdults') }}">
          <button type="button" class="btn btn-warning btn-lg mb-4 text-primary">
            Ver Gatos Adultos Disponibles
          </button>
        </a>
      </h4>
    </div>
    <div class="col-12 col-md-7">
      @include('components.carousel.IndexHero')
    </div>
  </div>
</div>

<div class="container-fluid bg-white">
  <div class="row d-flex text-center align-items-center justify-content-center">
    <div class="col-12 col-md-6 pt-5 pb-5">
      <img data-src="/imgs/heros/allure_bengals_cat_60.jpg" class="d-block w-100 rounded lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    <div class="col-12 col-md-6 mt-5 mb-5">
      <h2 class="mb-3 text-title">Allure Bengals da la bienvenida a todos</h2>
      <p><b>Buscando el gatito perfecto de Bengala?</b> Allure Bengals es una pequeña casa familiar ubicada en las colinas de Virgina.
        Tenemos algunas parejas próximas que nos entusiasman mucho.
        Nuestros gatitos vendrán con una garantía de salud del 100%, así como una garantía de defectos genéticos de por vida.
        Nuestros gatos y gatitos son criados en nuestro hogar. <br>Dejamos que nuestros gatitos nos dejen entre ocho y doce semanas.
        <br><br> Nuestro Cattery está registrado en TICA. <br><br>
        <img data-src="/imgs/logos/TICA.jpg" class="lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com"><br><br>
        ¡También pasan por diferentes etapas de desarrollo mientras están aquí con nosotros para garantizar una mascota de calidad!
        Nuestra familia tiene una pasión por los Bengals. Un amor por cómo estos gatos pueden cambiar tu vida. Familia con maravillosas mascotas hermosas.
        Si desea obtener más información sobre las literas y reservas actuales, contáctenos a continuación. ¡Gracias!
      </p>
    </div>
  </div>
</div>

<div class="container-fluid mt-5 pt-5 text-light text-center">
  <h3 class="mb-5">Información de Contacto</h3>
  <div class="row">
    <div class="col d-flex align-items-center justify-content-center my-2">
      <i class="fas fa-envelope fa-2x"></i> &nbsp; &nbsp; <a class="text-light" href="mailto:allurebengals@gmail.com"> allurebengals@gmail.com </a>
    </div>
  </div>
  <div class="row">
    <div class="col d-flex align-items-center justify-content-center my-2">
      <i class="fas fa-phone-alt fa-2x text-light"></i> &nbsp; &nbsp; <h4> (919) 308-4684 (Hablar con Zack)</h4>
    </div>
  </div>
  <div class="row">
    <div class="col d-flex align-items-center justify-content-center my-2">
      <i class="fas fa-phone-alt fa-2x text-light"></i> &nbsp; &nbsp; <h4> (276) 732-0277 (Hablar con Evyenia)</h4>
    </div>
  </div>
  <div class="row">
    <div class="col d-flex align-items-center justify-content-center my-2">
      <i class="fab fa-facebook-square fa-2x"></i> &nbsp; &nbsp; <a class="text-light" href="http://www.facebook.com/allurebengals"> facebook.com/allurebengals </a>
    </div>
  </div>
  <div class="row">
    <div class="col d-flex align-items-center justify-content-center my-2">
      <i class="fab fa-instagram fa-2x"></i> &nbsp; &nbsp; <a class="text-light" href="https://www.instagram.com/allurebengals/"> instagram.com/allurebengals/ </a>
    </div>
  </div>
  <div class="row">
    <div class="col d-flex align-items-center justify-content-center my-2">
      <i class="fab fa-twitter-square fa-2x"></i> &nbsp; &nbsp; <a class="text-light" href="https://twitter.com/AllureBengals"> twitter.com/AllureBengals </a>
    </div>
  </div>
  <div class="row">
    <div class="col d-flex align-items-center justify-content-center my-2">
      <p>El transporte aéreo está disponible en los EE. UU. E internacionalmente. Brindamos transporte aéreo seguro y muy confiable a través de Petsafe, Delta y United Airlines.</p>
    </div>
  </div>
</div>

@include('components.esFooter')
@endsection
