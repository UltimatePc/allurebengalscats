    @extends('layouts.guest')

    @section('title', 'Adults')
    @section('id', 'Allure Bengals')
    @section('pageName', 'Adults')
    @section('langSwitch', '/Adultos')

    @include('components.navbar', ['active' => 'Adults'])

    @section('content')

    <div class="container-fluid mt-5">
    <div class="row mb-3 mb-md-0 no-gutters">
    <div class="col">
        <h1 class="text-light mb-5 text-center text-title">Available Adults</h1>
    </div>
    </div>

    <!-- Calleum -->
    <div class="row mb-3 mb-md-0 no-gutters bg-light">
    <div class="col-12 col-md-6 text-black d-flex align-items-center">
    <div class="container text-box text-center text-md-right mb-5">
        <h1 class="m-5 text-title">Calleum</h1>
        <p class="mx-md-5">
        Sire- Bengal Island Cats Winterberry<br>
        Dam- Leopards Realm Matisse of Atlantic<br>
        Description- Calleum is a stunning tri color brown spotted rosetted male. He has
        been neutered. He is a very gentle boy; loves being petted and has meticulous
        litter box habits. He would do very well in any family home environment. Calleum
        is approximately 5 years old.
        </p>
    </div>
    </div>
    <div class="col-12 col-md-6 order-first order-md-last">
        <img src="/imgs/adults/Allure_Bengals_Calleum.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    </div>

    <!-- Emma -->
    <div class="row mb-3 mb-md-0 no-gutters">
    <div class="col-12 col-md-6">
        <img src="/imgs/adults/Allure_Bengals_Emma.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    <div class="col-12 col-md-6 text-light d-flex align-items-center">
        <div class="container text-box text-center text-md-left mb-5">
        <h1 class="m-5 text-title">Emma</h1>
        <p class="mx-md-5">
        Sire- Raffa<br>
        Dam- Bella<br>
        Description- Emma is a gorgeous brown spotted female. She is approximately 6
        years old. She has a very wild Bengal head and distinct brown spotted rosettes
        that are laid out in a perfect horizontal pattern. She is a sweet and very loving girl.
        </p>
        </div>
    </div>
    </div>

    <!-- Mocha -->
    <div class="row mb-3 mb-md-0 no-gutters bg-light">
    <div class="col-12 col-md-6 text-black d-flex align-items-center">
        <div class="container text-box text-center text-md-right mb-5">
        <h1 class="m-5 text-title">Mocha</h1>
        <p class="mx-md-5">
        Sire- London<br>
        Dam- Naudia<br>
        Description- Mocha is a dark brown spotted Bengal female. She is approximately a
        year old. Her whisker pads are amazing and huge. Her personality is so sweet.
        Mocha rolls around asking for pets. She will make a great addition to any family
        situation.
        </p>
        </div>
    </div>
    <div class="col-12 col-md-6 order-first order-md-last">
        <img src="/imgs/adults/Allure_Bengals_Mocha.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    </div>

    <!-- Sasha -->
    <div class="row mb-3 mb-md-0 no-gutters">
        <div class="col-12 col-md-6">
        <img src="/imgs/adults/Allure_Bengals_Sasha.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
        </div>
        <div class="col-12 col-md-6 text-light d-flex align-items-center">
        <div class="container text-box text-center text-md-left mb-5">
            <h1 class="m-5 text-title">Sasha</h1>
            <p class="mx-md-5">
            Sire- Jasmine<br>
            Dam- Orion<br>
            Description- Sasha is around a year and a half old. She is a tri color rosetted
            female Bengal. Her rosettes are ringed and have a rare ginger color on the inside.
            She is a very relaxed Bengal not dominant. She has a slight foot defect due to an
            umbilical cord issue during delivery. She has an intact foot and has fairly good
            mobility. This does not restrict her daily life in any way. She would do best in a
            single cat household.
            </p>
        </div>
        </div>
    </div>

    <!-- Sadie -->
    <div class="row mb-3 mb-md-0 no-gutters bg-light">
    <div class="col-12 col-md-6 text-black d-flex align-items-center">
        <div class="container text-box text-center text-md-right mb-5">
        <h1 class="m-5 text-title">Sadie</h1>
        <p class="mx-md-5">
        Sire- Jasmine<br>
        Dam- Orion<br>
        Description- Sadie is a beautiful brown rosetted female Bengal with a ginger tone.
        She is very friendly and has a great temperament. Sadie loves pets, treats and a
        good play session. She is approximately 2 years old.
        </p>
        </div>
    </div>
    <div class="col-12 col-md-6">
        <img src="/imgs/adults/Allure_Bengals_Sadie.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    </div>
    </div>

    @include('components.footer')
    @endsection
