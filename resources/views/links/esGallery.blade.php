@extends('layouts.guest')

@section('title', 'Galeria')
@section('id', 'Allure Bengals')
@section('pageName', 'Galeria')
@section('langSwitch', '/Gallery')

@include('components.esNavbar', ['active' => 'Galería'])

@section('content')

<div class="container-fluid mt-5">

  <!-- Row 1 -->
  <div class="card-group text-light text-center">
    <div class="card bg-dark">
      <img data-src="/imgs/gallery/allure_bengals_rogue.JPG" class="card-img-top rounded lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-md-block">
      </div>
    </div>
    <div class="card bg-dark">
      <img data-src="/imgs/gallery/allure_bengals_rogue_6.JPG" class="card-img-top rounded lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-md-block">
      </div>
    </div>
    <div class="card bg-dark">
      <img data-src="/imgs/gallery/allure_bengals_baby_4.jpg" class="card-img-top rounded lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-md-block">
      </div>
    </div>
  </div>

  <!-- Row 2 -->
  <div class="card-group text-light text-center">
    <div class="card bg-dark">
      <img data-src="/imgs/gallery/allure_bengals_rogue_2.JPG" class="card-img-top rounded lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-md-block">
      </div>
    </div>
    <div class="card bg-dark">
      <img data-src="/imgs/gallery/allure_bengals_baby.jpg" class="card-img-top rounded lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-md-block">
      </div>
    </div>
    <div class="card bg-dark">
      <img data-src="/imgs/gallery/allure_bengals_rogue_3.JPG" class="card-img-top rounded lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-md-block">
      </div>
    </div>
  </div>

  <!-- Row 3 -->
  <div class="card-group text-light text-center">
    <div class="card bg-dark">
      <img data-src="/imgs/gallery/allure_bengals_rogue_4.JPG" class="card-img-top rounded lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-md-block">
      </div>
    </div>
    <div class="card bg-dark">
      <img data-src="/imgs/gallery/allure_bengals_baby_2.jpg" class="card-img-top rounded lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-md-block">
      </div>
    </div>
    <div class="card bg-dark">
      <img data-src="/imgs/gallery/allure_bengals_rogue_5.JPG" class="card-img-top rounded lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-md-block">
      </div>
    </div>
  </div>

  <!-- Row 4 -->
  <div class="card-group text-light text-center">
    <div class="card bg-dark">
      <img data-src="/imgs/gallery/allure_bengals_rogue_7.JPG" class="card-img-top rounded lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-md-block">
      </div>
    </div>
    <div class="card bg-dark">
      <img data-src="/imgs/gallery/allure_bengals_bella.jpg" class="card-img-top rounded lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-md-block">
      </div>
    </div>
    <div class="card bg-dark">
      <img data-src="/imgs/gallery/allure_bengals_lavender_2.jpg" class="card-img-top rounded lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-md-block">
      </div>
    </div>
  </div>

  <!-- Row 5 -->
  <div class="card-group text-light text-center">
    <div class="card bg-dark">
      <img data-src="/imgs/gallery/allure_bengals_lavender_3.jpg" class="card-img-top rounded lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-md-block">
      </div>
    </div>
    <div class="card bg-dark">
      <img data-src="/imgs/heros/allure_bengals_cat_12.JPG" class="card-img-top rounded lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-md-block">
      </div>
    </div>
    <div class="card bg-dark">
      <img data-src="/imgs/heros/allure_bengals_cat_11.JPG" class="card-img-top rounded lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-md-block">
      </div>
    </div>
  </div>

  <!-- Row 6 -->
  <div class="card-group text-light text-center">
    <div class="card bg-dark">
      <img data-src="/imgs/heros/allure_bengals_cat_10.JPG" class="card-img-top rounded lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-md-block">
      </div>
    </div>
    <div class="card bg-dark">
      <img data-src="/imgs/heros/allure_bengals_cat_9.JPG" class="card-img-top rounded lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-md-block">
      </div>
    </div>
    <div class="card bg-dark">
      <img data-src="/imgs/heros/allure_bengals_cat_8.JPG" class="card-img-top rounded lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-md-block">
      </div>
    </div>
  </div>

  <!-- Row 7 -->
  <div class="card-group text-light text-center">
    <div class="card bg-dark">
      <img data-src="/imgs/heros/allure_bengals_cat_5.JPG" class="card-img-top rounded lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-md-block">
      </div>
    </div>
    <div class="card bg-dark">
      <img data-src="/imgs/heros/allure_bengals_cat_6.JPG" class="card-img-top rounded lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-md-block">
      </div>
    </div>
    <div class="card bg-dark">
      <img data-src="/imgs/heros/allure_bengals_cat_7.JPG" class="card-img-top rounded lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-md-block">
      </div>
    </div>
  </div>

  <!-- Row 8 -->
  <div class="card-group text-light text-center">
    <div class="card bg-dark">
      <img data-src="/imgs/heros/allure_bengals_cat_1.JPG" class="card-img-top rounded lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-md-block">
      </div>
    </div>
    <div class="card bg-dark">
      <img data-src="/imgs/heros/allure_bengals_cat_2.JPG" class="card-img-top rounded lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-md-block">
      </div>
    </div>
    <div class="card bg-dark">
      <img data-src="/imgs/heros/allure_bengals_cat_4.JPG" class="card-img-top rounded lazy" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-md-block">
      </div>
    </div>
  </div>

</div>

@include('components.esFooter')
@endsection
