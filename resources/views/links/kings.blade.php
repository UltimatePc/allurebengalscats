@extends('layouts.guest')

@section('title', 'Kings')
@section('id', 'Allure Bengals')
@section('pageName', 'Kings')
@section('langSwitch', '/Reyes')

@include('components.navbar', ['active' => 'Kings'])

@section('content')

<div class="container-fluid mt-5">
  <div class="row mb-3 mb-md-0 mb-3 mb-md-0 no-gutters">
    <div class="col">
      <h1 class="text-light mb-5 text-center text-title">King's</h1>
    </div>
  </div>

  <!-- Orion -->
  <div class="row mb-3 mb-md-0 no-gutters" id="Orion">
    <div class="col-12 col-md-6">
    <img src="/imgs/kings/allure_bengals_orion.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    <div class="col-12 col-md-6 text-light d-flex align-items-center">
      <div class="container text-box text-center text-md-left mb-5">
        <h1 class="m-5 text-title">Leopards Realm "Orion"</h1>
        <p class="mx-md-5">
        Orion is a stunning charcoal male. Which we were forunate to have Orion entrusted to us for care by Leopards Realm. 
        Orion is a large and relaxed Bengal with friendly demeanor. He is producing some incredible kittens with various colors, shapes, patterns of Charcoal, 
        Brown Spotted and Melanistic colors. Each kitten has Orion's Signature large Rosettes. 
        </p>
      </div>
    </div>
  </div>

  <!-- Oscar -->
  <div class="row mb-3 mb-md-0 no-gutters bg-light" id="Oscar">
    <div class="col-12 col-md-6 text-black d-flex align-items-center">
      <div class="container text-box text-center text-md-right mb-5">
        <h1 class="m-5 text-title">Allure Bengals "Oscar"</h1>
        <p class="mx-md-5">
        Oscar is an extremely rare solid mink snow bengal. 
        He has bright blue eyes against a unique coat color, reminiscent of a Siamese Oscar gets along well with adults cats of both genders. 
        We are just beginning to add him to our program and look foward to his offspring.
        </p>
      </div>
    </div>
    <div class="col-12 col-md-6 order-first order-md-last">
    <img src="/imgs/kings/allure_bengals_oscar.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
  </div>

  <!-- Cisco -->
  <div class="row mb-3 mb-md-0 no-gutters" id="Orion">
    <div class="col-12 col-md-6">
    <img src="/imgs/kings/allure_bengals_cisco.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    <div class="col-12 col-md-6 text-light d-flex align-items-center">
      <div class="container text-box text-center text-md-left mb-5">
        <h1 class="m-5 text-title">Allure Bengals "Cisco"</h1>
        <p class="mx-md-5">
        Cisco is a brown spotted Bengal with tri color rosseting. He has a perfect head extraordinary contrast and very strong structure. 
        He is a result of our continued passion to produce a perfect bengal. 
        We are very proud to have such a wonderful boy and friend and are very excited to see him continue to improve our breeding with our new queens in 2021
        </p>
      </div>
    </div>
  </div>

  <!-- London -->
  <div class="row mb-3 mb-md-0 no-gutters bg-light" id="Oscar">
    <div class="col-12 col-md-6 text-black d-flex align-items-center">
      <div class="container text-box text-center text-md-right mb-5">
        <h1 class="m-5 text-title">Allure Bengals "London of Atl"</h1>
        <p class="mx-md-5">
        London is a one of a kind Charcoal snow Bengal. We are just thrilled to have him. 
        With his friendly fiesty attitude and stunning features he brings alot to our breeding program. 
        He is one of our larger Bengals we have in our program but he is just a big furry ball of Joy! We are very excited to see him father some kittens!
        </p>
      </div>
    </div>
    <div class="col-12 col-md-6 order-first order-md-last">
    <img src="/imgs/kings/allure_bengals_london.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
  </div>

</div>

@include('components.footer')
@endsection
