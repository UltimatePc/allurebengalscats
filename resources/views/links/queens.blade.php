@extends('layouts.guest')

@section('title', 'Queens')
@section('id', 'Allure Bengals')
@section('pageName', 'Queens')
@section('langSwitch', '/Reinas')

@include('components.navbar', ['active' => 'Queens'])

@section('content')

<div class="container-fluid mt-5">
  <div class="row mb-3 mb-md-0 no-gutters">
    <div class="col">
      <h1 class="text-light mb-5 text-center text-title">Queens's</h1>
    </div>
  </div>

    <!-- Cha Cha -->
    <div class="row mb-3 mb-md-0 no-gutters bg-light" id="ChaCha">
    <div class="col-12 col-md-6 text-black d-flex align-items-center">
      <div class="container text-box text-center text-md-right mb-5">
        <h1 class="m-5 text-title">Cha Cha</h1>
        <p class="mx-md-5">
        Cha Cha is one of our charcoal Queens. She is out of Raven and Orion. 
        She has amazing charcoal kittens and is a wonderful mother. 
        Cha cha loves to have conversations and has such a wonderful temperament. 
        We always look forward to her charcoal babies they are such a joy
        </p>
      </div>
    </div>
    <div class="col-12 col-md-6 order-first order-md-last">
      @include('components.carousel.QueensChacha')
    </div>
  </div>

  <!-- Khaleesi -->
  <div class="row mb-3 mb-md-0 no-gutters" id="Khaleesi">
    <div class="col-12 col-md-6">
      @include('components.carousel.QueensKhaleesi')
    </div>
    <div class="col-12 col-md-6 text-light d-flex align-items-center">
      <div class="container text-box text-center text-md-left mb-5">
        <h1 class="m-5 text-title">Khaleesi</h1>
        <p class="mx-md-5">
          Allures Mother of Dragons Khaleesi.
          Her strong personality and temperament have made her one of our favorite Queens.
          Her kittens carry all the wonderdul characteristics and personality traits she has.
        </p>
      </div>
    </div>
  </div>

  <!-- Tessa -->
  <div class="row mb-3 mb-md-0 no-gutters bg-light" id="Tessa">
    <div class="col-12 col-md-6 text-black d-flex align-items-center">
      <div class="container text-box text-center text-md-right mb-5">
        <h1 class="m-5 text-title">Tessa</h1>
        <p class="mx-md-5">
          ...
        </p>
      </div>
    </div>
    <div class="col-12 col-md-6 order-first order-md-last">
      @include('components.carousel.QueensTessa')
    </div>
  </div>

  <!-- Bella -->
  <div class="row mb-3 mb-md-0 no-gutters" id="Bella">
    <div class="col-12 col-md-6">
      @include('components.carousel.QueensBella')
    </div>
    <div class="col-12 col-md-6 text-light d-flex align-items-center">
      <div class="container text-box text-center text-md-left mb-5">
        <h1 class="m-5 text-title">Bella</h1>
        <p class="mx-md-5">
          Bella is from our fellow breeders Hyponotic Exotics.
          Her lineage is very strong Bellas sire is Captain Jack Sparrow.
          She has amazing Charcoal kittens.
        </p>
      </div>
    </div>
  </div>

  <!-- Rosemary -->
  <div class="row mb-3 mb-md-0 no-gutters bg-light" id="Rosemary">
    <div class="col-12 col-md-6 text-black d-flex align-items-center">
      <div class="container text-box text-center text-md-right mb-5">
        <h1 class="m-5 text-title">Rosemary</h1>
        <p class="mx-md-5">
           Rosemary is a brown rossetted queen.
           She has outstanding contrast and color.
           Her pelt has a very natural flow.
           Not to mention she is a very loving a sweet girl.
           We are delighted for her to be apart of our program.
        </p>
      </div>
    </div>
    <div class="col-12 col-md-6 order-first order-md-last">
      @include('components.carousel.QueensRosemary')
    </div>
  </div>

  <!-- Lavender -->
  <div class="row mb-3 mb-md-0 no-gutters" id="Lavender">
    <div class="col-12 col-md-6">
      @include('components.carousel.QueensLavender')
    </div>
    <div class="col-12 col-md-6 text-light d-flex align-items-center">
      <div class="container text-box text-center text-md-left mb-5">
        <h1 class="m-5 text-title">Lavender</h1>
        <p class="mx-md-5">
          ...
        </p>
      </div>
    </div>
  </div>

  <!-- Spice -->
  <div class="row mb-3 mb-md-0 no-gutters mb-5 bg-light" id="Spice">
    <div class="col-12 col-md-6 text-black d-flex align-items-center">
      <div class="container text-box text-center text-md-right mb-5">
        <h1 class="m-5 text-title">Spice</h1>
        <p class="mx-md-5">
          ...
        </p>
      </div>
    </div>
    <div class="col-12 col-md-6 order-first order-md-last">
      @include('components.carousel.QueensSpice')
    </div>
  </div>

  <!-- Jasmine -->
  <div class="row mb-3 mb-md-0 no-gutters" id="Jasmine">
    <div class="col-12 col-md-6">
      @include('components.carousel.QueensJasmine')
    </div>
    <div class="col-12 col-md-6 text-light d-flex align-items-center">
      <div class="container text-box text-center text-md-left mb-5">
        <h1 class="m-5 text-title">Jasmine</h1>
        <p class="mx-md-5">
          ...
        </p>
      </div>
    </div>
  </div>

  <!-- Raven -->
  <div class="row mb-3 mb-md-0 no-gutters mb-5 bg-light" id="Raven">
    <div class="col-12 col-md-6 text-black d-flex align-items-center">
      <div class="container text-box text-center text-md-right mb-5">
        <h1 class="m-5 text-title">Raven</h1>
        <p class="mx-md-5">
          ...
        </p>
      </div>
    </div>
    <div class="col-12 col-md-6 order-first order-md-last">
      @include('components.carousel.QueensRaven')
    </div>
  </div>

</div>

@include('components.footer')
@endsection
