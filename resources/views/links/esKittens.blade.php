@extends('layouts.guest')

@section('title', 'Gatitos')
@section('id', 'Allure Bengals')
@section('pageName', 'Gatitos')
@section('langSwitch', '/Kittens')

@include('components.esNavbar', ['active' => 'Gatitos'])

@section('content')

<div class="container-fluid mt-5">
  <div class="row mb-3 mb-md-0 no-gutters">
  <div class="col">
    <h1 class="text-light mb-5 text-center text-title">Gatitos Disponibles</h1>
  </div>
  </div>

  <!-- Male #1 -->
  <div class="row mb-3 mb-md-0 no-gutters bg-light">
  <div class="col-12 col-md-6 text-black d-flex align-items-center">
    <div class="container text-box text-center text-md-right mb-5">
      <h1 class="m-5 text-title">Macho Rosetado Marrón #1</h1>
      <p class="mx-md-5">
      Naudia And London tuvo 4 hermosos gatitos color carbón con rosetas marrones el 7 de junio. Naudia está siendo una gran madre como siempre. 
      Tienen hermosas marcas con muy alto contraste. No podemos esperar a ver crecer a estos pequeños. 
      ¡Por favor contáctenos para disponibilidad!
      </p>
    </div>
  </div>
  <div class="col-12 col-md-6 order-first order-md-last">
  <img src="/imgs/kittens/Allure_Bengals_Male_1.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
  </div>
  </div>

  <!-- Male #2 -->
  <div class="row mb-3 mb-md-0 no-gutters">
  <div class="col-12 col-md-6">
  <img src="/imgs/kittens/Allure_Bengals_Male_2.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
  </div>
  <div class="col-12 col-md-6 text-light d-flex align-items-center">
    <div class="container text-box text-center text-md-left mb-5">
      <h1 class="m-5 text-title">Macho Rosetado Marrón #2</h1>
      <p class="mx-md-5">
      Sire- Cisco<br>
      Dam- Maya<br>
      Description- This male is one of our most stunning kittens available. 
      He is easily breeder quality due to his color, pattern and head type. 
      His personality like his Sire Cisco, very easy going, so loveable as a pet. 
      He is approximately 5 months old
      </p>
    </div>
  </div>
  </div>

  <!-- Male #3 -->
  <div class="row mb-3 mb-md-0 no-gutters bg-light">
    <div class="col-12 col-md-6 text-black d-flex align-items-center">
      <div class="container text-box text-center text-md-right mb-5">
        <h1 class="m-5 text-title">Gatito Macho #3</h1>
        <p class="mx-md-5">
        Sire- Prince<br>
        Dam- Rosemary<br>
        Description- This little guy is 4 weeks old. 
        He has a wonderful personality and temperament for his age. 
        He Has a little spice to him. Bunny hops around and is currently going through his developmental stages well.
        </p>
    </div>
  </div>
    <div class="col-12 col-md-6 order-first order-md-last">
      <img src="/imgs/kittens/Allure_Bengals_Male_3.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
  </div>

  <!-- Male #4 -->
  <div class="row mb-3 mb-md-0 no-gutters">
    <div class="col-12 col-md-6">
    <img src="/imgs/kittens/Allure_Bengals_Male_4.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    <div class="col-12 col-md-6 text-light d-flex align-items-center">
      <div class="container text-box text-center text-md-left mb-5">
        <h1 class="m-5 text-title">Gatito Macho #4</h1>
        <p class="mx-md-5">
        Sire- Prince<br>
        Dam- Rosemary<br>
        Description- This baby boy is a litter mate to kitten #3. 
        He also is developing very well. 
        They love to play together and would do good together If you were looking for two kittens. 
        He has a great temperament and will make a wonderful pet.
        </p>
      </div>
    </div>
  </div>
</div>

@include('components.esFooter')
@endsection
