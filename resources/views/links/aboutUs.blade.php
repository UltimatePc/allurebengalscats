@extends('layouts.guest')

@section('title', 'About Us')
@section('id', 'Allure Bengals')
@section('pageName', 'About Us')
@section('langSwitch', '/Nosotros')

@include('components.navbar', ['active' => 'About Us'])

@section('content')

<div class="container-fluid mt-5">

  <div class="row no-gutters my-5">
    <div class="col">
      <h1 class="text-light text-center text-title">About Us</h1>
    </div>
  </div>

<div class="container text-center">
    <h4 class="text-white mt-5">
    Welcome to Allure Bengals Website. We appreciate you taking out the time to view our cats and kittens. Allure Bengals is a TICA registered cattery. Our family has been working with Bengal cats for close to twenty years now. Our primary goal has always been to produce superior quality Bengals with excellent temperaments. This accomplished by selective breeding. We pick qualities both in structure and personality to help produce higher quality Bengals.
    <br><br>Along the way we have had mentors that helped guide us and select the best breeder cats to work with. We express our extreme gratitude for being mentored by some of the top Bengal breeders in the nation. We thank you. 
    <br><br>This past year we relocated and bought a home in Honaker Va. which is in the gorgeous mountains of Virginia. Allure Bengals services in northern Virginia, Maryland, North Carolina, Tennessee, West Virginia and South Carolina. We also will transport nationally.  We breed on an extremely high level of quality. Our Breeder cats are screened yearly for condition’s affecting the Bengal Cat and cat illnesses in general. An important difference between Allure Bengals and other cat breeders is we offer a 100% health guarantee. This includes a lifetime genetic defect guarantee unheard of in most catteries. We offer this guarantee for one because we have high degree of confidence that our Bengals are free from disease as our lines we have worked with for years. But if anything were to unexpectedly arise Allure Bengals is committed to providing you with a healthy cat or kitten.
    <br><br>We love our cats dearly. They are all considered our pets. Kittens are raised inside with us in a loving home environment. We spend most of the day caring and socializing the youngsters from birth. Your kitten will be up to date on vaccinations and be dewormed. Kittens are fully litter boxed trained prior to leaving us. They go through different developmental stages while they are here with us to ensure a well socialized kitten. We never let a kitten leave until it is ready both physically and socially. Reservations are 500 dollars per kitten. Adoption Fees are from 1800 to 3500 dollars. We offer military discounts and multiple kitten discounts.  If you are looking for an older cat, check to see if we have any retiring cats available. They are the cream of the crop!
    <br><br>If you are looking to be placed on the Reservation List or if you have any questions. We look Forward to hearing from you!
    <br>
    <br><br>Thank You,
    <br><br>Zachary Lexington Dunlap
    <br>(919)308-4684
    <br>Honaker V.A
    </h4>
    <h4 class="text-white mt-5">
      -Allure Bengals
    </h4>
    <h4 class="m-5">
      <a href="mailto:Allurebengals@gmail.com" class="text-white">Allurebengals@gmail.com</a>
    </h4>
  </div>
</div>

@include('components.esFooter')
@endsection
