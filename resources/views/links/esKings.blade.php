@extends('layouts.guest')

@section('title', 'Reyes')
@section('id', 'Allure Bengals')
@section('pageName', 'Reyes')
@section('langSwitch', '/Kings')

@include('components.esNavbar', ['active' => 'Reyes'])

@section('content')

<div class="container-fluid mt-5">
  <div class="row mb-3 mb-md-0 mb-3 mb-md-0 no-gutters">
    <div class="col">
      <h1 class="text-light mb-5 text-center text-title">Los Reyes</h1>
    </div>
  </div>

  <!-- Orion -->
  <div class="row mb-3 mb-md-0 no-gutters" id="Orion">
    <div class="col-12 col-md-6">
    <img src="/imgs/kings/allure_bengals_orion.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    <div class="col-12 col-md-6 text-light d-flex align-items-center">
      <div class="container text-box text-center text-md-left mb-5">
        <h1 class="m-5 text-title">Leopards Realm "Orion"</h1>
        <p class="mx-md-5">
        Orion es un impresionante macho de carbón. Que estábamos forzados a que Orion nos confiara el Reino Leopardos.
          Orion es una Bengala grande y relajada con un comportamiento amigable. Él está produciendo algunos gatitos increíbles con varios colores, formas, patrones de carbón, marrón manchado y colores melanísticos.
          Cada gatito tiene Rosetones grandes exclusivos de Orion.
        </p>
      </div>
    </div>
  </div>

  <!-- Oscar -->
  <div class="row mb-3 mb-md-0 no-gutters bg-light" id="Oscar">
    <div class="col-12 col-md-6 text-black d-flex align-items-center">
      <div class="container text-box text-center text-md-right mb-5">
        <h1 class="m-5 text-title">Allure Bengals "Oscar"</h1>
        <p class="mx-md-5">
        Oscar es un visón sólido extremadamente raro de nieve de bengala.
          Tiene ojos azules brillantes contra un color de pelaje único, que recuerda a un siamés
          Oscar se lleva bien con los gatos adultos de ambos sexos.
          Recién estamos comenzando a agregarlo a nuestro programa y mirar hacia adelante a su descendencia.
        </p>
      </div>
    </div>
    <div class="col-12 col-md-6 order-first order-md-last">
    <img src="/imgs/kings/allure_bengals_oscar.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
  </div>

  <!-- Cisco -->
  <div class="row mb-3 mb-md-0 no-gutters" id="Orion">
    <div class="col-12 col-md-6">
    <img src="/imgs/kings/allure_bengals_cisco.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    <div class="col-12 col-md-6 text-light d-flex align-items-center">
      <div class="container text-box text-center text-md-left mb-5">
        <h1 class="m-5 text-title">Allure Bengals "Cisco"</h1>
        <p class="mx-md-5">
        Cisco es un Bengala manchado de marrón con rosseting tricolor. Tiene una cabeza perfecta de extraordinario contraste y una estructura muy fuerte.
        Es el resultado de nuestra continua pasión por producir una bengala perfecta.
        Estamos muy orgullosos de tener un chico y amigo tan maravilloso y estamos muy emocionados de verlo continuar mejorando nuestra cría con nuestras nuevas reinas en 2021.
        </p>
      </div>
    </div>
  </div>

  <!-- London -->
  <div class="row mb-3 mb-md-0 no-gutters bg-light" id="Oscar">
    <div class="col-12 col-md-6 text-black d-flex align-items-center">
      <div class="container text-box text-center text-md-right mb-5">
        <h1 class="m-5 text-title">Allure Bengals "London of Atl"</h1>
        <p class="mx-md-5">
        Londres es una Bengala de nieve de carbón única en su tipo. Estamos encantados de tenerlo.
        Con su actitud amistosa fiesty y características sorprendentes, aporta mucho a nuestro programa de cría.
        Es uno de nuestros Bengals más grandes que tenemos en nuestro programa, ¡pero es solo una gran bola peluda de Joy! ¡Estamos muy emocionados de verlo engendrar algunos gatitos!
        </p>
      </div>
    </div>
    <div class="col-12 col-md-6 order-first order-md-last">
    <img src="/imgs/kings/allure_bengals_london.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
  </div>

</div>

@include('components.esFooter')
@endsection
