<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
  <div class="container">
    <a class="navbar-brand" href="{{ route('index') }}">
      <img data-src="/imgs/logos/allure_bengals_mini_logo.png" class="lazy" alt="Allure Bengal Cats">
    </a>
    <h4 class="mt-3 text-light">Allure<br>Bengals</h4>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-center" id="navbarNavDropdown">
      <ul class="navbar-nav text-center">
        <li class="nav-item{{ isset($active) && $active == 'Home' ? ' active' : ''}}">
          <a class="nav-link" href="{{ route('index') }}">Home</a>
        </li>
        <li class="nav-item{{ isset($active) && $active == 'Kittens' ? ' active' : ''}}">
          <a class="nav-link" href="{{ route('kittens') }}">Kittens</a>
        </li>
        <li class="nav-item{{ isset($active) && $active == 'Adults' ? ' active' : ''}}">
          <a class="nav-link" href="{{ route('adults') }}">Adults</a>
        </li>
        <li class="nav-item{{ isset($active) && $active == 'Kings' ? ' active' : ''}}">
          <a class="nav-link" href="{{ route('kings') }}">Kings</a>
        </li>
        <li class="nav-item{{ isset($active) && $active == 'Queens' ? ' active' : ''}}">
          <a class="nav-link" href="{{ route('queens') }}">Queens</a>
        </li>
        <li class="nav-item{{ isset($active) && $active == 'Gallery' ? ' active' : ''}}">
          <a class="nav-link" href="{{ route('gallery') }}">Gallery</a>
        </li>
        <li class="nav-item{{ isset($active) && $active == 'Contact Us' ? ' active' : ''}}">
          <a class="nav-link" href="{{ route('contactUs') }}">Contact Us</a>
        </li>
        <li class="nav-item{{ isset($active) && $active == 'About Us' ? ' active' : ''}}">
          <a class="nav-link" href="{{ route('aboutUs') }}">About Us</a>
        </li>
      </ul>
      <a class="nav-link text-center pl-lg-5 ml-lg-5" href="@yield('langSwitch')">
        <button type="button" class="btn btn-light">
          Español
        </button>
      </a>
    </div>
  </div>
</nav>
