<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
  <div class="container">
    <a class="navbar-brand" href="{{ route('index') }}">
      <img data-src="/imgs/logos/allure_bengals_mini_logo.png" class="lazy" alt="Allure Bengal Cats">
    </a>
    <h4 class="mt-3 text-light">Allure<br>Bengals</h4>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-center" id="navbarNavDropdown">
      <ul class="navbar-nav text-center">
        <li class="nav-item{{ isset($active) && $active == 'Inicio' ? ' active' : ''}}">
          <a class="nav-link" href="{{ route('esIndex') }}">Inicio</a>
        </li>
        <li class="nav-item{{ isset($active) && $active == 'Gatitos' ? ' active' : ''}}">
          <a class="nav-link" href="{{ route('esKittens') }}">Gatitos</a>
        </li>
        <li class="nav-item{{ isset($active) && $active == 'Gatos Adultos' ? ' active' : ''}}">
          <a class="nav-link" href="{{ route('esAdults') }}">Gatos Adultos</a>
        </li>
        <li class="nav-item{{ isset($active) && $active == 'Reyes' ? ' active' : ''}}">
          <a class="nav-link" href="{{ route('esKings') }}">Reyes</a>
        </li>
        <li class="nav-item{{ isset($active) && $active == 'Reinas' ? ' active' : ''}}">
          <a class="nav-link" href="{{ route('esQueens') }}">Reinas</a>
        </li>
        <li class="nav-item{{ isset($active) && $active == 'Galería' ? ' active' : ''}}">
          <a class="nav-link" href="{{ route('esGallery') }}">Galería</a>
        </li>
        <li class="nav-item{{ isset($active) && $active == 'Contáctenos' ? ' active' : ''}}">
          <a class="nav-link" href="{{ route('esContactUs') }}">Contáctenos</a>
        </li>
        <li class="nav-item{{ isset($active) && $active == 'De Nosotros' ? ' active' : ''}}">
          <a class="nav-link" href="{{ route('esAboutUs') }}">De Nosotros</a>
        </li>
      </ul>
      <a class="nav-link text-center pl-lg-5 ml-lg-5" href="@yield('langSwitch')">
        <button type="button" class="btn btn-light">
          English
        </button>
      </a>
    </div>
  </div>
</nav>
