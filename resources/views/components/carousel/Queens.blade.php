<div id="carouselQueens" class="carousel slide carousel-fade" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselQueens" data-slide-to="0" class="active"></li>
    <li data-target="#carouselQueens" data-slide-to="1"></li>
    <li data-target="#carouselQueens" data-slide-to="2"></li>
    <li data-target="#carouselQueens" data-slide-to="3"></li>
    <li data-target="#carouselQueens" data-slide-to="4"></li>
    <li data-target="#carouselQueens" data-slide-to="5"></li>
    <li data-target="#carouselQueens" data-slide-to="6"></li>
    <li data-target="#carouselQueens" data-slide-to="7"></li>
    <li data-target="#carouselQueens" data-slide-to="8"></li>
    <li data-target="#carouselQueens" data-slide-to="9"></li>
    <li data-target="#carouselQueens" data-slide-to="10"></li>
    <li data-target="#carouselQueens" data-slide-to="11"></li>
    <li data-target="#carouselQueens" data-slide-to="12"></li>
    <li data-target="#carouselQueens" data-slide-to="13"></li>
    <li data-target="#carouselQueens" data-slide-to="14"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="/imgs/queens/allure_bengals_cleopatra.png" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Cleopatra</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img src="/imgs/queens/allure_bengals_cleopatra_2.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Cleopatra</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img src="/imgs/queens/allure_bengals_ginger.png" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Ginger</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img src="/imgs/queens/allure_bengals_jazzime.png" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Jazzime</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img src="/imgs/queens/allure_bengals_jazzime_2.png" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Jazzime</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img src="/imgs/queens/allure_bengals_jazzime_3.png" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Jazzime</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img src="/imgs/queens/allure_bengals_lavender.png" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Lavender</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img src="/imgs/queens/allure_bengals_lavender_2.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Lavender</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img src="/imgs/queens/allure_bengals_lavender_3.png" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Lavender</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img src="/imgs/queens/allure_bengals_leilani.png" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Leilani</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img src="/imgs/queens/allure_bengals_luna.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Luna</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img src="/imgs/queens/allure_bengals_luna_2.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Luna</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img src="/imgs/queens/allure_bengals_olivia.png" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Olivia</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img src="/imgs/queens/allure_bengals_rosemary.png" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Rosemary</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img src="/imgs/queens/allure_bengals_tessa.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title text-dark">Tessa</h1>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselQueens" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselQueens" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
