<div id="carouselKittensMale1" class="carousel slide carousel-fade" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselKittensMale1" data-slide-to="0" class="active"></li>
    <li data-target="#carouselKittensMale1" data-slide-to="1"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="/imgs/kittens/allure_bengals_male1.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    <div class="carousel-item">
      <img src="/imgs/kittens/allure_bengals_male1.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselKittensMale1" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselKittensMale1" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
