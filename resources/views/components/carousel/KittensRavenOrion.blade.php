<div id="carouselKittensRavenOrion" class="carousel slide carousel-fade" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselKittensRavenOrion" data-slide-to="0" class="active"></li>
    <li data-target="#carouselKittensRavenOrion" data-slide-to="1"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="/imgs/available/allure_bengals_raven_5.JPG" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Mother: Raven</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img src="/imgs/kings/allure_bengals_orion.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Father: Orion</h1>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselKittensRavenOrion" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselKittensRavenOrion" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
