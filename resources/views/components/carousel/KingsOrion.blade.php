<div id="carouselKingsOrion" class="carousel slide carousel-fade" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselKingsOrion" data-slide-to="0" class="active"></li>
    <li data-target="#carouselKingsOrion" data-slide-to="1"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="/imgs/kings/allure_bengals_orion.jpg" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Orion</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img src="/imgs/kings/allure_bengals_orion.JPG" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Orion</h1>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselKingsOrion" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselKingsOrion" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
