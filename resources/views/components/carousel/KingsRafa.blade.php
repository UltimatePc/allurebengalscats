<div id="KingRafa" class="carousel slide carousel-fade" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#KingRafa" data-slide-to="0" class="active"></li>
    <li data-target="#KingRafa" data-slide-to="1"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="/imgs/kings/allure_bengals_rafa.JPG" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Rafa</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img src="/imgs/kings/allure_bengals_rafa.JPG" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Rafa</h1>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#KingRafa" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#KingRafa" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
