<div id="allureCatIndexHero" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#IndexHero" data-slide-to="0" class="active"></li>
    <li data-target="#IndexHero" data-slide-to="1"></li>
    <li data-target="#IndexHero" data-slide-to="2"></li>
    <li data-target="#IndexHero" data-slide-to="3"></li>
    <li data-target="#IndexHero" data-slide-to="4"></li>
    <li data-target="#IndexHero" data-slide-to="5"></li>
    <li data-target="#IndexHero" data-slide-to="6"></li>
    <li data-target="#IndexHero" data-slide-to="7"></li>
    <li data-target="#IndexHero" data-slide-to="8"></li>
    <li data-target="#IndexHero" data-slide-to="9"></li>
    <li data-target="#IndexHero" data-slide-to="10"></li>
    <li data-target="#IndexHero" data-slide-to="11"></li>
  </ol>
  <div class="carousel-inner rounded">
    <div class="carousel-item active">
      <img src="/imgs/available/Allure_Bengals_Naudia_Litter1.JPG" class="d-block w-100" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">*Litter Announcement*</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img src="/imgs/heros/allure_bengals_cat_1.JPG" class="d-block w-100" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    <div class="carousel-item">
      <img src="/imgs/heros/allure_bengals_cat_2.JPG" class="d-block w-100" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    <div class="carousel-item">
      <img src="/imgs/heros/allure_bengals_cat_3.JPG" class="d-block w-100" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    <div class="carousel-item">
      <img src="/imgs/heros/allure_bengals_cat_4.JPG" class="d-block w-100" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    <div class="carousel-item">
      <img src="/imgs/heros/allure_bengals_cat_5.JPG" class="d-block w-100" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    <div class="carousel-item">
      <img src="/imgs/heros/allure_bengals_cat_6.JPG" class="d-block w-100" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    <div class="carousel-item">
      <img src="/imgs/heros/allure_bengals_cat_7.JPG" class="d-block w-100" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    <div class="carousel-item">
      <img src="/imgs/heros/allure_bengals_cat_8.JPG" class="d-block w-100" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    <div class="carousel-item">
      <img src="/imgs/heros/allure_bengals_cat_9.JPG" class="d-block w-100" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    <div class="carousel-item">
      <img src="/imgs/heros/allure_bengals_cat_10.JPG" class="d-block w-100" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    <div class="carousel-item">
      <img src="/imgs/heros/allure_bengals_cat_11.JPG" class="d-block w-100" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
    <div class="carousel-item">
      <img src="/imgs/heros/allure_bengals_cat_12.JPG" class="d-block w-100" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
    </div>
  </div>
  <a class="carousel-control-prev" href="#allureCatIndexHero" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#allureCatIndexHero" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
