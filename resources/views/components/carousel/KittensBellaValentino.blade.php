<div id="carouselKittensBellaValentino" class="carousel slide carousel-fade" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselKittensBellaValentino" data-slide-to="0" class="active"></li>
    <li data-target="#carouselKittensBellaValentino" data-slide-to="1"></li>
    <li data-target="#carouselKittensBellaValentino" data-slide-to="2"></li>
    <li data-target="#carouselKittensBellaValentino" data-slide-to="3"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="/imgs/heros/allure_bengals_cat_9.JPG" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
      </div>
    </div>
    <div class="carousel-item">
      <img src="/imgs/heros/allure_bengals_cat_10.JPG" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
      </div>
    </div>
    <div class="carousel-item">
      <img src="/imgs/heros/allure_bengals_cat_11.JPG" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
      </div>
    </div>
    <div class="carousel-item">
      <img src="/imgs/heros/allure_bengals_cat_12.JPG" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselKittensBellaValentino" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselKittensBellaValentino" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
