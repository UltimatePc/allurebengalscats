<div id="carouselKittenFemale1" class="carousel slide carousel-fade" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="/imgs/available/Allure_Bengals_Naudia_Brown_Rosetted_Female_1.JPG" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <p class="mx-md-5 d-block d-md-none text-info text-center">
        Female #1
      </p>
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Female #1</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img src="/imgs/available/Allure_Bengals_Naudia_Brown_Rossetted_Female_1a.JPG" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <p class="mx-md-5 d-block d-md-none text-info text-center">
        Female #1
      </p>
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Female #1</h1>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselKittenFemale1" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselKittenFemale1" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>