<div id="QueensRaven" class="carousel slide carousel-fade" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#QueensRaven" data-slide-to="0" class="active"></li>
    <li data-target="#QueensRaven" data-slide-to="1"></li>
    <li data-target="#QueensRaven" data-slide-to="2"></li>
    <li data-target="#QueensRaven" data-slide-to="3"></li>
    <li data-target="#QueensRaven" data-slide-to="4"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="/imgs/available/allure_bengals_raven.JPG" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Raven</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img src="/imgs/available/allure_bengals_raven_2.JPG" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Raven</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img src="/imgs/available/allure_bengals_raven_3.JPG" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Raven</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img src="/imgs/available/allure_bengals_raven_4.JPG" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Raven</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img src="/imgs/available/allure_bengals_raven_5.JPG" class="d-block w-100 rounded" alt="Allure Bengals, Bengal Cats, AllureBengals.com">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="text-title">Raven</h1>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#QueensRaven" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#QueensRaven" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
