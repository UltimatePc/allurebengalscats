<div class="container-fluid mb-5 bg-light">
  <div class="row">
    <div class="col-12 col-md-5 text-center">
      <h2 class="text-title mt-4">Our Resources</h2>
      <p>We are greatful for all your help and support to make us what we are today</p>
    </div>
    <div class="col-12 col-md-7">
      <div id="allureCatIndexHero" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner rounded">
          <div class="carousel-item active">
            <div class="d-flex justify-content-center">
              <a href="http://www.bengalcat.com/">
                <img data-src="/imgs/resources/tibcs.png" class="d-block w-100 lazy" alt="Allure Bengal Cats">
              </a>
            </div>
          </div>
          <div class="carousel-item">
            <div class="d-flex justify-content-center">
              <a href="https://www.bengalsillustrated.com/">
                <img data-src="/imgs/resources/bengals_illustrated.png" class="d-block w-100 lazy" alt="Allure Bengal Cats">
              </a>
            </div>
          </div>
          <div class="carousel-item">
            <div class="d-flex justify-content-center">
              <a href="http://breedersdirectory.com/">
                <img data-src="/imgs/resources/breeders_directory.png" class="d-block w-100 lazy" alt="Allure Bengal Cats">
              </a>
            </div>
          </div>
          <div class="carousel-item">
            <div class="d-flex justify-content-center">
              <a href="http://www.fanciersplus.com/">
                <img data-src="/imgs/resources/fanciers_plus.png" class="d-block w-100 lazy" alt="Allure Bengal Cats">
              </a>
            </div>
          </div>
          <div class="carousel-item">
            <div class="d-flex justify-content-center">
              <a href="https://www.felines4us.com/">
                <img data-src="/imgs/resources/felines_4_us.png" class="d-block w-100 lazy" alt="Allure Bengal Cats">
              </a>
            </div>
          </div>
          <div class="carousel-item">
            <div class="d-flex justify-content-center">
              <a href="https://kittysites.com/">
                <img data-src="/imgs/resources/kitty_sites.png" class="d-block w-100 lazy" alt="Allure Bengal Cats">
              </a>
            </div>
          </div>
          <div class="carousel-item">
            <div class="d-flex justify-content-center">
              <a href="https://petsunlimited.eu/">
                <img data-src="/imgs/resources/pets_unlimited.png" class="d-block w-100 lazy" alt="Allure Bengal Cats">
              </a>
            </div>
          </div>
          <div class="carousel-item">
            <div class="d-flex justify-content-center">
              <a href="https://www.ringsurf.com/">
                <img data-src="/imgs/resources/ring_surf.png" class="d-block w-100 lazy" alt="Allure Bengal Cats">
              </a>
            </div>
          </div>
          <div class="carousel-item">
            <div class="d-flex justify-content-center">
              <a href="https://www.royalcanin.com/us">
                <img data-src="/imgs/resources/royal_canin.png" class="d-block w-100 lazy" alt="Allure Bengal Cats">
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
