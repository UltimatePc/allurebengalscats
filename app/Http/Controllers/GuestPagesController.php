<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GuestPagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('links.index');
    }

    public function available()
    {
        return view('links.available');
    }

    public function adults()
    {
        return view('links.adults');
    }

    public function disponible()
    {
        return view('links.disponible');
    }

    public function kittens()
    {
        return view('links.kittens');
    }

    public function kings()
    {
        return view('links.kings');
    }

    public function queens()
    {
        return view('links.queens');
    }

    public function gallery()
    {
        return view('links.gallery');
    }

    public function contactUs()
    {
        return view('links.contactUs');
    }

    public function aboutUs()
    {
        return view('links.aboutUs');
    }

    public function esIndex()
    {
        return view('links.esIndex');
    }

    public function esKittens()
    {
        return view('links.esKittens');
    }

    public function esAdults()
    {
        return view('links.esAdults');
    }

    public function esKings()
    {
        return view('links.esKings');
    }

    public function esQueens()
    {
        return view('links.esQueens');
    }

    public function esAboutUs()
    {
        return view('links.esAboutUs');
    }

    public function esGallery()
    {
        return view('links.esGallery');
    }

    public function esContactUs()
    {
        return view('links.esContactUs');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
